//
//  String+IVGFoundation.swift
//  IVGFoundation
//
//  Created by Douglas Sjoquist on 2/10/17.
//  Copyright © 2017 Ivy Gulch. All rights reserved.
//

import Foundation

public extension String {

    func stringByAppendingPathComponent(_ value: String) -> String {
        return (self as NSString).appendingPathComponent(value)
    }

    var attributedStringFromHTML: NSAttributedString? {
        guard let data = self.data(using: .utf16, allowLossyConversion: false) else { return nil }
        guard let result = try? NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else { return nil }
        return result
    }

    func stringFromHTML(stripNewLines: Bool = true) -> String? {
        let result = attributedStringFromHTML?.string
        return stripNewLines ? result?.replacingOccurrences(of: "\n", with: "") : result
    }

    func substring(withNSRange nsRange: NSRange) -> String? {
        guard let range = Range(nsRange) else { return nil }

        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(startIndex, offsetBy: range.upperBound)
        return String(describing: self[start..<end])
    }

}

public extension String {

    func isValid(withRegEx pattern: String) -> Bool {
        guard let regEx = try? NSRegularExpression(pattern: pattern, options: []) else { return false }

        let range = NSRange(location: 0, length: self.count)
        return !regEx.matches(in: self, options: [], range: range).isEmpty
    }

    static let emailRegEx = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$"

    var isValidEmail: Bool { return isValid(withRegEx: String.emailRegEx) }

}

// MARK: - a-z -
public extension String {
    var a: String { return self + "a" }
    var b: String { return self + "b" }
    var c: String { return self + "c" }
    var d: String { return self + "d" }
    var e: String { return self + "e" }
    var f: String { return self + "f" }
    var g: String { return self + "g" }
    var h: String { return self + "h" }
    var i: String { return self + "i" }
    var j: String { return self + "j" }
    var k: String { return self + "k" }
    var l: String { return self + "l" }
    var m: String { return self + "m" }
    var n: String { return self + "n" }
    var o: String { return self + "o" }
    var p: String { return self + "p" }
    var q: String { return self + "q" }
    var r: String { return self + "r" }
    var s: String { return self + "s" }
    var t: String { return self + "t" }
    var u: String { return self + "u" }
    var v: String { return self + "v" }
    var w: String { return self + "w" }
    var x: String { return self + "x" }
    var y: String { return self + "y" }
    var z: String { return self + "z" }
}

// MARK: - A-Z -
public extension String {
    var A: String { return self + "A" }
    var B: String { return self + "B" }
    var C: String { return self + "C" }
    var D: String { return self + "D" }
    var E: String { return self + "E" }
    var F: String { return self + "F" }
    var G: String { return self + "G" }
    var H: String { return self + "H" }
    var I: String { return self + "I" }
    var J: String { return self + "J" }
    var K: String { return self + "K" }
    var L: String { return self + "L" }
    var M: String { return self + "M" }
    var N: String { return self + "N" }
    var O: String { return self + "O" }
    var P: String { return self + "P" }
    var Q: String { return self + "Q" }
    var R: String { return self + "R" }
    var S: String { return self + "S" }
    var T: String { return self + "T" }
    var U: String { return self + "U" }
    var V: String { return self + "V" }
    var W: String { return self + "W" }
    var X: String { return self + "X" }
    var Y: String { return self + "Y" }
    var Z: String { return self + "Z" }
}

// MARK: - Numbers -
public extension String {
    var _1: String { return self + "1" }
    var _2: String { return self + "2" }
    var _3: String { return self + "3" }
    var _4: String { return self + "4" }
    var _5: String { return self + "5" }
    var _6: String { return self + "6" }
    var _7: String { return self + "7" }
    var _8: String { return self + "8" }
    var _9: String { return self + "9" }
    var _0: String { return self + "0" }
}

// MARK: - Punctuation -
public extension String {
    var space: String { return self + " " }
    var point: String { return self + "." }
    var dash: String { return self + "-" }
    var comma: String { return self + "," }
    var semicolon: String { return self + ";" }
    var colon: String { return self + ":" }
    var apostrophe: String { return self + "'" }
    var quotation: String { return self + "\"" }
    var plus: String { return self + "+" }
    var equals: String { return self + "=" }
    var paren_left: String { return self + "(" }
    var paren_right: String { return self + ")" }
    var asterisk: String { return self + "*" }
    var ampersand: String { return self + "&" }
    var caret: String { return self + "^" }
    var percent: String { return self + "%" }
    var `$`: String { return self + "$" }
    var pound: String { return self + "#" }
    var at: String { return self + "@" }
    var exclamation: String { return self + "!" }
    var question_mark: String { return self + "?" }
    var back_slash: String { return self + "\\" }
    var forward_slash: String { return self + "/" }
    var curly_left: String { return self + "{" }
    var curly_right: String { return self + "}" }
    var bracket_left: String { return self + "[" }
    var bracket_right: String { return self + "]" }
    var bar: String { return self + "|" }
    var less_than: String { return self + "<" }
    var greater_than: String { return self + ">" }
    var underscore: String { return self + "_" }
}

// MARK: - Aliases -
public extension String {
    var dot: String { return point }
}

//
//  TypeKey.swift
//  IVGFoundation
//
//  Created by Douglas Sjoquist on 3/19/16.
//  Copyright © 2016 Ivy Gulch LLC. All rights reserved.
//

import Foundation

public struct TypeKey: Hashable {
    public init<T>(_ keyType: T) {
        self.keyName = "\(keyType.self)"
    }

    public static func == (lhs: TypeKey, rhs: TypeKey) -> Bool {
        return lhs.keyName == rhs.keyName
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(keyName)
    }

    fileprivate var keyName: String
}

func ==(lhs: TypeKey, rhs: TypeKey) -> Bool {
    return lhs.keyName == rhs.keyName
}
